# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from typing import Optional

import os
import git
from portmod import use_reduce


class Git:
    """
    Pybuild Class that directly fetches from Git repos

    Subclasses should specify GIT_SRC_URI, containing a use-reduce-able
    list of remote git repositories
    Optionally, GIT_BRANCH, GIT_COMMIT and GIT_COMMIT_DATE can be used to specify
    what branch and commit should be used.
    """

    GIT_SRC_URI: str
    GIT_BRANCH: Optional[str] = None
    GIT_COMMIT: Optional[str] = None
    GIT_COMMIT_DATE: Optional[str] = None

    def src_unpack(self):
        repos = use_reduce(self.GIT_SRC_URI, self.USE, [], is_src_uri=True, flat=True)
        self.INSTALLED_COMMIT = {}

        for repo in repos:
            name, _ = os.path.splitext(os.path.basename(repo))
            if self.GIT_COMMIT:
                gitrepo = git.Repo.clone_from(repo, name)
                gitrepo.git.checkout(self.GIT_COMMIT)
            elif self.GIT_COMMIT_DATE:
                gitrepo = git.Repo.clone_from(repo, name)
                gitrepo.git.checkout(
                    "{}@{}".format(
                        self.GIT_BRANCH or gitrepo.active_branch.name,
                        "{" + self.GIT_COMMIT_DATE + "}",
                    )
                )
            elif self.GIT_BRANCH:
                gitrepo = git.Repo.clone_from(
                    repo, name, depth=1, shallow_submodules=True, branch=self.GIT_BRANCH
                )
            else:
                gitrepo = git.Repo.clone_from(
                    repo, name, depth=1, shallow_submodules=True
                )

            self.INSTALLED_COMMIT[repo] = gitrepo.head.object.hexsha

    def can_update_live(self):
        repos = use_reduce(self.GIT_SRC_URI, self.USE, [], is_src_uri=True, flat=True)

        g = git.cmd.Git()
        for repo in repos:
            newhash = g.ls_remote(repo).split()[0]
            oldhash = self.get_installed_env()["INSTALLED_COMMIT"][repo]
            if oldhash != newhash:
                return True

        return False
